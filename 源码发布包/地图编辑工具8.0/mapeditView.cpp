// mapeditView.cpp : implementation of the CMapeditView class
//

#include "stdafx.h"
#include "mapedit.h"
#include "mapeditDoc.h"
#include "mapeditView.h"
#include "ThumbnailDlg.h"

#include <atlimage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// 用于绘制界面的displaymap函数及其子函数 //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//displaymap函数中使用到如下全局变量
//	int center_x;
//	int center_y;//地图矩阵中当前操作位的坐标，值域0到199
//	int selected;//地图单元集中当前单元的号码，值域0到800
//	int cur_area;//当前显示的区号，值域1到4
void CMapeditView::displaymap(void)
{
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	//x；y：地图区域中当前编辑位置的坐标，值域（0~199）
	BuffDC.BitBlt(0,0,EDIT_AREA_W,EDIT_AREA_H,&TileDC,0,0,BLACKNESS);//地图外面的区域使用黑色

	//（在游戏中地图外面的区域使用黑色）
	draw_edit_area(pDoc, EDIT_AREA_GRID_W,EDIT_AREA_GRID_H,&BuffDC);//显示地图区域长21格，高21格。

	CDC *pdc = GetDC();
	pdc->SelectObject(m_ViewFont);
	draw_map_tiles(tile_area, &BuffDC);//在主缓上绘制地图单元格位图
	draw_table_lines(selected, &BuffDC);//在主缓上绘制分界线
	pdc->BitBlt(0,0,1024,672,&BuffDC,0,0,SRCCOPY);//将主缓送入屏幕

	//修改状态栏的显示内容
	pdc->BitBlt(0,INFO_BAR_Y,600,20,&BuffDC,0,0,WHITENESS);
	CString sEditInfo;
	sEditInfo.Format("编辑区中央格子位置： x= %d    y= %d", center_x, center_y);
	pdc->TextOut(10, INFO_BAR_Y, sEditInfo);

	if (m_bEditEventMode) {
		int m = pDoc->ReadEvt(center_x, center_y);
		if (m > 0) {
			sEditInfo.Format("code= %d", m);
			pdc->TextOut(360, INFO_BAR_Y, sEditInfo);
		}
	}
	else {
		int m = pDoc->ReadMap(center_x, center_y);
		if (m > 0) {
			sEditInfo.Format("tile= %d", m);
			pdc->TextOut(360, INFO_BAR_Y, sEditInfo);
		}
	}

	CString sTilePage;
	sTilePage.Format("第 %d 页，瓷砖编号 %d", tile_area, selected);
	pdc->TextOutA(MAP_TILES_LEFT + 70,
		MAP_TILES_TOP + MAP_TILES_H + SCROLL_BAR_W + 12,
		sTilePage);

	//刷新滚动条显示
	m_scoEditAreaH.Invalidate();
	m_scoEditAreaV.Invalidate();
	m_scoMapTilesH.Invalidate();
	ReleaseDC(pdc);
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::draw_edit_area(CMapeditDoc* pDoc, int lth,int hth,CDC *pbuffDC)
{//在主缓（*pbuffDC）内绘制长lth，高hth的区域（可以调节地图显示区域的大小）
	int a = (center_x - (EDIT_AREA_GRID_W / 2));//左上角的格子在地图矩阵中的坐标
	int b = (center_y - (EDIT_AREA_GRID_H / 2));
	int c = 0;//地图格在屏幕上所占据的矩形左上角的坐标，以32递加
	int d = 0;
	int i, m, n;
	for (m = 0; m < hth; m++)
	{
		for (n = 0; n < lth; n++)
		{
			if ((a < MAP_GRID_W) &&
				(a >= 0)         &&
				(b < MAP_GRID_H) &&
				(b >= 0))
			{
				//地图边界检查，只有在200*200范围内的才绘制，否则保持原有的黑色
				int m01=(a * MAP_TO_THUMBNAIL_SCALE);//这两个参数用于确定背景草稿的缩放比例
				int m02=(b * MAP_TO_THUMBNAIL_SCALE);//5表示：一个单元格对应草稿图的5个像素
				//flag001：//如果修改了5则flag002处的5,5,也要修改
				i = (pDoc->ReadMap(a,b));
				draw_map_ceil(pDoc, i,c,d,pbuffDC,m01,m02);

				if (m_bEditEventMode) {
					i = (pDoc->ReadEvt(a,b));
					draw_evt_ceil(pDoc, i,c,d,pbuffDC,m01,m02);
				}
			}
			a ++;
			c += TILE_W;
		}
		a = (center_x - (EDIT_AREA_GRID_W / 2));
		b ++;
		c = 0;
		d += TILE_H;
	}
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::draw_map_ceil(CMapeditDoc* pDoc, int i, int x, int y, CDC *pbuffDC, int m, int n)
{//用于显示地图的一个单元。参数：i：图片值；x，y：屏幕左上角坐标；m，n：背景草图左上角坐标
	if ((i <= MAP_TILES_NUM_MAX) && (i > 0)) {
		int sx = (((i - 1) % MAP_TILES_LOGIC_W) * TILE_W);
		int sy = (((i - 1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从0到800
		pbuffDC->BitBlt(x,y,TILE_W,TILE_H,&TileDC,sx,sy,SRCCOPY);//绘制地图
	}
	else {
		pbuffDC->StretchBlt(x,y,TILE_W,TILE_H,&MiniDC,m,n,
			MAP_TO_THUMBNAIL_SCALE,MAP_TO_THUMBNAIL_SCALE,SRCCOPY);//绘制背景草图
		//flag002：如果修改了5,5,则flag001处的两个5也要修改
	}
}
void CMapeditView::draw_evt_ceil(CMapeditDoc* pDoc, int i, int x, int y, CDC *pbuffDC, int m, int n)
{//用于显示地图的一个单元。参数：i：图片值；x，y：屏幕左上角坐标；m，n：背景草图左上角坐标
	if ((i <= MAP_TILES_NUM_MAX) && (i > 0)) {
		int sx = (((i - 1) % MAP_TILES_LOGIC_W) * TILE_W);
		int sy = (((i - 1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从1到800
		pbuffDC->BitBlt(x,y,TILE_W,TILE_H,&CodeDC,sx,sy,SRCCOPY);//绘制
	}
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::draw_map_tiles(int area_num, CDC *pbuffDC)
{//根据单元格位图编号，确定显示位图的区域
	int x = TILE_W * MAP_TILES_DISP_W * (area_num - 1);
	switch(area_num){
	case 1:break;
	case 2:break;
	case 3:break;
	case 4:break;
	default:
		CString sErr;
		sErr.Format("(Error-005)输入的区块编号超出了值域(1~4)\narea_num = %d", area_num);
		MessageBox(sErr,NULL,MB_OK);
		PostQuitMessage(0);
	}

	if (m_bEditEventMode) {
		pbuffDC->BitBlt(MAP_TILES_LEFT,MAP_TILES_TOP,
			TILE_W * MAP_TILES_DISP_W,
			TILE_W * MAP_TILES_DISP_H, &CodeDC,x,0,SRCCOPY);
	}
	else {
		pbuffDC->BitBlt(MAP_TILES_LEFT,MAP_TILES_TOP,
			TILE_W * MAP_TILES_DISP_W,
			TILE_W * MAP_TILES_DISP_H, &TileDC,x,0,SRCCOPY);
	}
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::draw_table_lines(int cilnum,CDC *pbuffDC)
{//该函数用于绘制所有的边界线
	int i, m;
	CRect rc;
	CPen* pOldPen = NULL;
	CPen whitePen;//白色画笔
	CPen greenPen;//绿色画笔
	CPen pinkPen;//粉红色画笔
	CPen redPen;//红色的画笔
	whitePen.CreatePen(PS_SOLID,1,RGB(255,255,255));
	greenPen.CreatePen(PS_SOLID,1,RGB(0,255,0));
	pinkPen.CreatePen(PS_SOLID,1,RGB(255,192,255));
	redPen.CreatePen(PS_SOLID,1,RGB(255,0,0));

	//在地图单元区绘制网格，区分单元之间的界限
	//可行走区域用绿色绘制
	if (!m_bEditEventMode)
	{
		pOldPen = pbuffDC->SelectObject(&greenPen);
		for (i = 0; i < MAP_TILES_DISP_H + 1; i++)
		{//绘制纵线
			pbuffDC->MoveTo((i * TILE_W + MAP_TILES_LEFT), MAP_TILES_TOP);
			pbuffDC->LineTo((i * TILE_W + MAP_TILES_LEFT),
				MAP_TILES_TOP + TILE_H * TILE_PASSABLE_ROW_COUNT);
		}
		for (i = 0; i < TILE_PASSABLE_ROW_COUNT + 1; i++)
		{//绘制横线
			pbuffDC->MoveTo(MAP_TILES_LEFT, (i * TILE_H + MAP_TILES_TOP));
			pbuffDC->LineTo(MAP_TILES_LEFT + MAP_TILES_W, (i * TILE_H + MAP_TILES_TOP));
		}

		//不可行走区域用灰色绘制
		pbuffDC->SelectObject(&pinkPen);
		for (i = 0; i < MAP_TILES_DISP_W + 1; i++)
		{//绘制纵线
			pbuffDC->MoveTo((i * TILE_W + MAP_TILES_LEFT),
				MAP_TILES_TOP + TILE_H * TILE_PASSABLE_ROW_COUNT);
			pbuffDC->LineTo((i * TILE_W + MAP_TILES_LEFT),
				MAP_TILES_TOP + TILE_H * MAP_TILES_DISP_H);
		}
		for (i = TILE_PASSABLE_ROW_COUNT + 1; i < MAP_TILES_DISP_H + 1; i++)
		{//绘制横线
			pbuffDC->MoveTo(MAP_TILES_LEFT, (i * TILE_H + MAP_TILES_TOP));
			pbuffDC->LineTo(MAP_TILES_LEFT + MAP_TILES_W, (i * TILE_H + MAP_TILES_TOP));
		}
	}

	//在编辑区的周围绘制红色矩形（根据当前的画笔大小进行绘制）
	rc.left   = TILE_W * (EDIT_AREA_GRID_W / 2);
	rc.top    = TILE_H * (EDIT_AREA_GRID_H / 2);
	rc.right  = rc.left + TILE_W;
	rc.bottom = rc.top + TILE_H;
	if (NULL == pOldPen) {
		pbuffDC->SelectObject(&greenPen);
	}
	else {
		pOldPen = pbuffDC->SelectObject(&greenPen);
	}
	drawrect(&rc,pbuffDC);
	if (! m_bEditEventMode) {
		for (m = 1; m < cur_brush; m++)
		{
			rc.left  -= TILE_W;
			rc.right += TILE_W;
			rc.top   -= TILE_H;
			rc.bottom+= TILE_H;
		}
		pbuffDC->SelectObject(&redPen);
		drawrect(&rc, pbuffDC);
	}

	//在选中的地图单元的周围绘制红色矩形
	if ((cilnum <= MAP_TILES_NUM_MAX) && (cilnum > 0))
	{
		cilnum--;
		int sx=(cilnum % MAP_TILES_LOGIC_W);
		int sy=(cilnum / MAP_TILES_LOGIC_W);//i的范围从1到800，转换成相对坐标
		while(sx > 9){//x坐标转化成屏幕显示范围之内的坐标
			sx -= 10;
		}
		rc.left   = (sx * TILE_W + MAP_TILES_LEFT);//计算当前地图单元的区域
		rc.top    = (sy * TILE_H + MAP_TILES_TOP);
		rc.right  = rc.left + TILE_W;
		rc.bottom = rc.top + TILE_H;
		if (m_bEditEventMode) {
			pbuffDC->SelectObject(&whitePen);
		}
		drawrect(&rc,pbuffDC);//在当前地图单元的四周绘制红色的矩形
	}

	pbuffDC->SelectObject(pOldPen);//恢复画笔

	//删除画笔
	DeleteObject(whitePen);
	DeleteObject(greenPen);
	DeleteObject(pinkPen);
	DeleteObject(redPen);
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::drawrect(CRect *prc,CDC *pbuffDC)
{
	//用于绘制一个空心矩形
	pbuffDC->MoveTo(prc->left,prc->top);
	pbuffDC->LineTo(prc->right,prc->top);
	pbuffDC->LineTo(prc->right,prc->bottom);
	pbuffDC->LineTo(prc->left,prc->bottom);
	pbuffDC->LineTo(prc->left,prc->top);
}
/////////////////////////////////////////////////////////////////////////////
// displaymap函数及其子函数结束 ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMapeditView

IMPLEMENT_DYNCREATE(CMapeditView, CView)

BEGIN_MESSAGE_MAP(CMapeditView, CView)
	//{{AFX_MSG_MAP(CMapeditView)
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_UP, OnStepUp)
	ON_COMMAND(ID_DOWN, OnStepDown)
	ON_COMMAND(ID_LEFT, OnStepLeft)
	ON_COMMAND(ID_RIGHT, OnStepRight)
	ON_COMMAND(ID_SET, OnSet)
	ON_COMMAND(ID_ERASE, OnErase)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_COMMAND(ID_THUMBNAIL, OnThumbnail)
	ON_COMMAND(ID_EDIT_EVENT, OnEditEventMode)
	ON_UPDATE_COMMAND_UI(ID_EDIT_EVENT, OnUpdateEditEventMode)
	ON_COMMAND(ID_OPEN_EVENT_FILE, OnOpenEventFile)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_PICKUP, OnPickup)
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_BRUSH1, OnBrush1)
	ON_UPDATE_COMMAND_UI(ID_BRUSH1, OnUpdateBrush1)
	ON_COMMAND(ID_BRUSH2, OnBrush2)
	ON_UPDATE_COMMAND_UI(ID_BRUSH2, OnUpdateBrush2)
	ON_COMMAND(ID_BRUSH3, OnBrush3)
	ON_UPDATE_COMMAND_UI(ID_BRUSH3, OnUpdateBrush3)
	ON_COMMAND(ID_BRUSH4, OnBrush4)
	ON_UPDATE_COMMAND_UI(ID_BRUSH4, OnUpdateBrush4)
	ON_COMMAND(ID_BRUSH5, OnBrush5)
	ON_UPDATE_COMMAND_UI(ID_BRUSH5, OnUpdateBrush5)
	ON_COMMAND(ID_BRUSH6, OnBrush6)
	ON_UPDATE_COMMAND_UI(ID_BRUSH6, OnUpdateBrush6)
	ON_COMMAND(ID_BRUSH7, OnBrush7)
	ON_UPDATE_COMMAND_UI(ID_BRUSH7, OnUpdateBrush7)
	ON_COMMAND(ID_QUICK_DOWN, OnQuickDown)
	ON_COMMAND(ID_QUICK_LEFT, OnQuickLeft)
	ON_COMMAND(ID_QUICK_RIGHT, OnQuickRight)
	ON_COMMAND(ID_QUICK_UP, OnQuickUp)
	ON_COMMAND(ID_STEP_RIGHT, OnStepRight)
	ON_COMMAND(ID_STEP_DOWN, OnStepDown)
	ON_COMMAND(ID_STEP_LEFT, OnStepLeft)
	ON_COMMAND(ID_STEP_UP, OnStepUp)
	ON_COMMAND(ID_COPY, OnCopy)
	ON_UPDATE_COMMAND_UI(ID_COPY, OnUpdateCopy)
	ON_COMMAND(ID_PASTE, OnPaste)
	ON_UPDATE_COMMAND_UI(ID_PASTE, OnUpdatePaste)
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapeditView construction/destruction

CMapeditView::CMapeditView()
{
	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;
	m_bEditEventMode = FALSE;
}

CMapeditView::~CMapeditView()
{
}

BOOL CMapeditView::PreCreateWindow(CREATESTRUCT& cs)
{
	TCHAR path[MAX_PATH];
	strncpy(path, theApp.m_pszHelpFilePath, MAX_PATH);
	int	j=strlen(path);
	for(;path[j]!='\\';j--);
	path[j+1]='\0';
	CString sModelPath = path;
	CString sFileMapTile = sModelPath + "tilemap.bmp";
	CString sFileMiniMap = sModelPath + "minimap.bmp";
	if (! PathFileExists(sFileMapTile)) {
		MessageBox("应用程序目录下未发现 tilemap.bmp 文件！");
		return FALSE;
	}
	if (! PathFileExists(sFileMiniMap)) {
		MessageBox("应用程序目录下未发现 mimimap.bmp 文件！");
		return FALSE;
	}
	m_pMapTile=(HBITMAP)LoadImage(NULL,sFileMapTile,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//地图单元格位图
	m_pMiniMap=(HBITMAP)LoadImage(NULL,sFileMiniMap,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//调入地图草稿
	run_once = TRUE;//保证OnDraw函数中的初始化段落只执行一次
	center_x = (EDIT_AREA_GRID_W / 2);//当前编辑单元的坐标(中心位置)
	center_y = (EDIT_AREA_GRID_H / 2);
	tile_area =1;//当前地图选择区的编号（1~4）
	selected =1;//当前地图的编号
	cur_brush =1;//当前画刷的编号
	for(int i=0;i<19;i++){
		for(int j=0;j<19;j++){
			buffer[i][j]=0;//清空复制缓冲区（剪切板）
		}
	}

	m_ViewFont.CreateFont(16, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));

	
	//  the CREATESTRUCT cs
	return CView::PreCreateWindow(cs);
}

CString CMapeditView::GetPathName(void)
{
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	return pDoc->GetPathName();
}

/////////////////////////////////////////////////////////////////////////////
// CMapeditView drawing

void CMapeditView::OnDraw(CDC* pDC)
{
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	if (run_once) {//程序第一次运行所需的操作

		m_Buff.CreateCompatibleBitmap(pDC,1024,672);

		CBitmap codemap;
		codemap.LoadBitmap(IDB_CODEMAP);

		TileDC.CreateCompatibleDC(pDC);
		TileDC.SelectObject(m_pMapTile);
		CodeDC.CreateCompatibleDC(pDC);
		CodeDC.SelectObject(&codemap);
		MiniDC.CreateCompatibleDC(pDC);
		MiniDC.SelectObject(m_pMiniMap);
		BuffDC.CreateCompatibleDC(pDC);
		BuffDC.SelectObject(&m_Buff);//初始化各个DC
		pDoc->ResetBackup();
		pDoc->undo_st=FALSE;//复位剪切板及其参数
		run_once = FALSE;
	}
	displaymap();//用于重绘界面
	//ReleaseDC(pdc);
}

/////////////////////////////////////////////////////////////////////////////
// CMapeditView diagnostics

#ifdef _DEBUG
void CMapeditView::AssertValid() const
{
	CView::AssertValid();
}

void CMapeditView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMapeditDoc* CMapeditView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMapeditDoc)));
	return (CMapeditDoc*)m_pDocument;
}
#endif //_DEBUG
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMapeditView message handlers
//███鼠标消息███████████████████████████████
void CMapeditView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	int mx=point.x;
	int my=point.y;
	if ((mx>MAP_TILES_LEFT) &&
		(mx<MAP_TILES_LEFT+MAP_TILES_W) &&
		(my>MAP_TILES_TOP) &&
		(my<MAP_TILES_TOP+MAP_TILES_H))
	{
		mx=((mx - MAP_TILES_LEFT) / TILE_W);
		my=((my - MAP_TILES_TOP)  / TILE_H);
		//计算鼠标着陆位置
		switch(tile_area){
		case 1:selected=(my*MAP_TILES_LOGIC_W+mx+1);break;//1区
		case 2:selected=(my*MAP_TILES_LOGIC_W+mx+11);break;//2区
		case 3:selected=(my*MAP_TILES_LOGIC_W+mx+21);break;//3区
		case 4:selected=(my*MAP_TILES_LOGIC_W+mx+31);break;//4区
		default:break;
		}//根据区号计算所选图形的编号
		displaymap();
	}

	// 标记鼠标左键已经落下
	m_bLButtonDown = TRUE;
	m_bRButtonDown = FALSE;

	// 将鼠标命中的单元设置为当前选中的格子
	SetOneCell (point.x, point.y, selected);

	CView::OnLButtonDown(nFlags, point);
}

void CMapeditView::OnRButtonDown(UINT nFlags, CPoint point) 
{//右键点击清除格子
	// 标记鼠标右键已经落下
	m_bLButtonDown = FALSE;
	m_bRButtonDown = TRUE;

	// 清掉鼠标命中的单元
	SetOneCell (point.x, point.y, 0);

	CView::OnRButtonDown(nFlags, point);
}
//███单步移动███████████████████████████████
void CMapeditView::OnStepDown() 
{//地图向下一单元格
	center_y +=1;
	if (center_y > MAP_GRID_H-1) {center_y = MAP_GRID_H-1;}//边界检查
	m_scoEditAreaV.SetScrollPos(center_y + 1);
	displaymap();
}

void CMapeditView::OnStepLeft() 
{//地图向左一单元格
	center_x -=1;
	if (center_x < 0) {center_x = 0;}//边界检查
	m_scoEditAreaH.SetScrollPos(center_x + 1);
	displaymap();
}

void CMapeditView::OnStepRight() 
{//地图向右一单元格
	center_x +=1;
	if (center_x > MAP_GRID_W-1) {center_x = MAP_GRID_W-1;}//边界检查
	m_scoEditAreaH.SetScrollPos(center_x + 1);
	displaymap();
}

void CMapeditView::OnStepUp() 
{//地图向上一单元格
	center_y -=1;
	if (center_y < 0) {center_y = 0;}//边界检查
	m_scoEditAreaV.SetScrollPos(center_y + 1);
	displaymap();
}
//███快速移动████████████████████████████████
void CMapeditView::OnQuickDown() 
{//快速向下，一步10格
	center_y += EDIT_PAGE_STEP;
	if (center_y > MAP_GRID_H-1) {center_y = MAP_GRID_H-1;}
	m_scoEditAreaV.SetScrollPos(center_y + 1);
	displaymap();
}

void CMapeditView::OnQuickLeft() 
{//快速向左，一步10格
	center_x -= EDIT_PAGE_STEP;
	if (center_x < 0) {center_x = 0;}
	m_scoEditAreaH.SetScrollPos(center_x + 1);
	displaymap();
}

void CMapeditView::OnQuickRight() 
{//快速向右，一步10格
	center_x += EDIT_PAGE_STEP;
	if (center_x > MAP_GRID_W-1) {center_x = MAP_GRID_W-1;}
	m_scoEditAreaH.SetScrollPos(center_x + 1);
	displaymap();
}

void CMapeditView::OnQuickUp() 
{//快速向上，一步10格
	center_y -= EDIT_PAGE_STEP;
	if(center_y < 0) {center_y = 0;}
	m_scoEditAreaV.SetScrollPos(center_y + 1);
	displaymap();
}
//███以下是关于放置和撤销的函数██████████████████████
void CMapeditView::OnSet() 
{//放置单元格
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int w = (cur_brush * 2 - 1);//计算画刷的边长，单位：格
	int i, j;
	for (i = 0; i < w; i++) {//按照画刷的大小进行放置
		for (j = 0; j < w; j++) {
			int k = (center_x - cur_brush + 1 + j);
			int l = (center_y - cur_brush + 1 + i);
			if(k<0){k=0;}//位置有效性检查
			if(k>MAP_GRID_W-1){k=MAP_GRID_W-1;}
			if(l<0){l=0;}
			if(l>MAP_GRID_H-1){l=MAP_GRID_W-1;}
			if (m_bEditEventMode) {
				pDoc->WriteEvt(k,l,selected);//写入事件矩阵
			}
			else {
				pDoc->WriteMap(k,l,selected);//写入地图矩阵
			}
		}
	}
	displaymap();
}

void CMapeditView::OnErase()
{//清除单元格
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int w = (cur_brush * 2 - 1);//计算画刷的边长，单位：格
	int i, j;
	for (i = 0; i < w; i++) {//按照画刷的大小进行放置
		for (j = 0; j < w; j++) {
			int k = (center_x - cur_brush + 1 + j);
			int l = (center_y - cur_brush + 1 + i);
			if(k<0){k=0;}//位置有效性检查
			if(k>MAP_GRID_W-1){k=MAP_GRID_W-1;}
			if(l<0){l=0;}
			if(l>MAP_GRID_H-1){l=MAP_GRID_W-1;}
			if (m_bEditEventMode) {
				pDoc->WriteEvt(k,l,0);//写入事件矩阵
			}
			else {
				pDoc->WriteMap(k,l,0);//写入地图矩阵
			}
		}
	}
	displaymap();
}

void CMapeditView::OnThumbnail()
{//显示缩略图
	CThumbnailDlg dlg(this);
	dlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
void CMapeditView::OnPickup() 
{//吸取当前单元格
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int m = 0;
	if (m_bEditEventMode) {
		m = pDoc->ReadEvt(center_x, center_y);
	}
	else {
		m = pDoc->ReadMap(center_x, center_y);
	}
	selected = m;
	tile_area = ((m-1) % MAP_TILES_LOGIC_W) / MAP_TILES_DISP_W +1;
	m_scoMapTilesH.SetScrollPos(tile_area);
	displaymap();
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::OnEditUndo() 
{//撤销一个格子的绘制
	if (m_bEditEventMode) {
		return;//事件编辑没有undo功能
	}

	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pDoc->Undo();
	displaymap();
}
//███画刷选择███████████████████████████████
void CMapeditView::OnBrush1() 
{//1号刷子（1*1）
	cur_brush =1;
	displaymap();
}

void CMapeditView::OnBrush2() 
{//2号刷子（3*3）
	cur_brush =2;
	displaymap();
}

void CMapeditView::OnBrush3() 
{//3号刷子（5*5）
	cur_brush =3;
	displaymap();
}

void CMapeditView::OnBrush4() 
{//4号刷子（7*7）
	cur_brush =4;
	displaymap();
}

void CMapeditView::OnBrush5() 
{//5号刷子（9*9）
	cur_brush =5;
	displaymap();
}

void CMapeditView::OnBrush6() 
{//6号刷子（11*11）
	cur_brush =6;
	displaymap();
}

void CMapeditView::OnBrush7() 
{//7号刷子（19*19）
	cur_brush =10;
	displaymap();
}
//██复制和粘贴███████████████████████████████
void CMapeditView::OnCopy()
{//复制
	if (m_bEditEventMode) {
		return;//事件编辑没有copy功能
	}

	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int i;
	for(i=0;i<19;i++){//清空剪切板
		for(int j=0;j<19;j++){
			buffer[i][j]=0;
		}
	}
	int a = center_x - 9;
	int b = center_y - 9;
	for(i=0;i<19;i++){//不论画刷大小，一律按照最大的画刷复制（粘贴时再限制尺寸）
		for(int j=0;j<19;j++){
			if(!((a<0)||(a>MAP_GRID_W-1)||(b<0)||(b>MAP_GRID_H-1))){
				buffer[i][j]=(pDoc->ReadMap(a,b));
			}
			a++;
		}
		a = center_x - 9;
		b ++;
	}
}
/////////////////////////////////////////////////////////////////////////////
void CMapeditView::OnPaste()
{//粘贴，selected==0的单元将跳过而不被粘贴
	if (m_bEditEventMode) {
		return;//事件编辑没有paste功能
	}

	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int m=(10-cur_brush);//剪切板中对应当前画刷左上角单元的x坐标和y坐标（二者相同）
	int n=(cur_brush*2-1);//画刷的边长（单位：格）
	int l=(center_x-cur_brush+1);//地图矩阵中当前单元的x坐标
	int a=l;
	int b=(center_y-cur_brush+1);//地图矩阵中当前单元的y坐标
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(!((a<0)||(a>MAP_GRID_W-1)||(b<0)||(b>MAP_GRID_H-1))){
				if((buffer[m+i][m+j]<=MAP_TILES_NUM_MAX)&&(buffer[m+i][m+j]>0)){//剪切版内容有效性检查
					pDoc->WriteMap(a,b,buffer[m+i][m+j]);
				}
			}
		a++;
		}
	a=l;
	b++;
	}
	displaymap();
}

static const int mini_tile_size = 4;

void CMapeditView::GetThumbnailSize(int* w, int* h)
{
	*w = MAP_GRID_W * mini_tile_size;
	*h = MAP_GRID_H * mini_tile_size;
}

void CMapeditView::GetThumbnail(CDC* pDC) 
{//该函数仅用于显示地图的缩略图。请在打印机中将纸型改为A2
	//显示方式同绘图，比例缩小，每单元格取样4*4像素
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	BeginWaitCursor();

	pDC->StretchBlt(0, 0,
		MAP_GRID_W * mini_tile_size, MAP_GRID_H * mini_tile_size,
		&MiniDC, 0, 0, THUMBNAIL_W, THUMBNAIL_H, SRCCOPY);

	int scale_x = TILE_W / mini_tile_size;
	int scale_y = TILE_H / mini_tile_size;

	int a = 0;
	int b = 0;
	int i, m, n;
	for (m = 0; m < MAP_GRID_W; m++) {
		for (n = 0; n < MAP_GRID_H; n++) {
			i = pDoc->ReadMap(m, n);
			if (i > 0 && i <= MAP_TILES_NUM_MAX) {
				int sx = (((i-1) % MAP_TILES_LOGIC_W) * TILE_W);
				int sy = (((i-1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从0到800
				//pDC->StretchBlt(b,a,mini_tile_size,mini_tile_size,&TileDC,sx,sy,32,32,SRCCOPY);//绘制
				//StretchBlt函数虽然速度快，但是压缩效果太差，还不如截取部分进行拼图
				pDC->BitBlt(b,a,mini_tile_size,mini_tile_size,&TileDC,sx,sy,SRCCOPY);//绘制
			}
			//else{
			//	pDC->BitBlt(b,a,mini_tile_size,mini_tile_size,&TileDC,0,0,WHITENESS);//地图的边界区域使用白色
			//}

			if (m_bEditEventMode) {
				//事件很小，看不清数字，统一绘制成黑色方块
				int e = pDoc->ReadEvt(m, n);
				if (e > 0 && e <= MAP_TILES_NUM_MAX) {
					int sx = (((e-1) % MAP_TILES_LOGIC_W) * TILE_W);
					int sy = (((e-1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从0到800
					pDC->BitBlt(b,a,mini_tile_size,mini_tile_size,&TileDC,sx,sy,BLACKNESS);//绘制
				}
			}

			a += mini_tile_size;
		}
		a = 0;
		b += mini_tile_size;
	}

	EndWaitCursor();
}

void CMapeditView::SetCenterPos(int x, int y)
{
	if (((x < 0) || (x > MAP_GRID_W-1) || (y < 0) || (y > MAP_GRID_H-1))) {
		return;
	}
	center_x = x;
	center_y = y;
	displaymap();
}

//██与事件编辑有关的函数█████████████████████████
void CMapeditView::OnEditEventMode()
{
	if (m_bEditEventMode)
	{//关闭事件编辑模式
		selected = 0;
		tile_area = 1;
		m_bEditEventMode = FALSE;
		displaymap();
	}
	else
	{//开启事件编辑模式

		CMapeditDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);

		if (pDoc->m_sEventFileName.IsEmpty())
		{
			if (IDOK == MessageBox("尚未指定 npc 文件的名称，需要创建新文件吗？", "提示", MB_OKCANCEL))
			{
				//选择存储文件的位置
				CString filter = "文件 (*.npc)|*.npc||";	//文件过滤的类型
				CFileDialog fileDlg(false, "npc", "*.npc", OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, filter, NULL);
				fileDlg.GetOFN().lpstrInitialDir = pDoc->GetPathName();
				if (IDOK == fileDlg.DoModal()) {
					pDoc->m_sEventFileName = fileDlg.GetPathName();
				}
				else {
					return;
				}
			}
			else {//如果用户不打算创建新文件，那么就取消操作
				return;
			}
		}

		cur_brush = 1;
		selected = 0;
		tile_area = 1;
		m_bEditEventMode = TRUE;
		displaymap();
	}
}

void CMapeditView::OnOpenEventFile()
{
	//如果当前文件没有存盘，先提示是否存盘
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	if (! pDoc->m_sEventFileName.IsEmpty() && pDoc->m_bEventModified)
	{
		if (IDYES == MessageBox("是否保存当前的 npc 文件？", "提示", MB_YESNO))
		{
			pDoc->SaveEventFile();
		}
	}

	//选择存储文件的位置
	CString filter = "文件 (*.npc)|*.npc||";	//文件过滤的类型
	CFileDialog fileDlg(true, "npc", "*.npc", OFN_HIDEREADONLY|OFN_READONLY, filter, NULL);
	fileDlg.GetOFN().lpstrInitialDir = pDoc->GetPathName();
	if (IDOK == fileDlg.DoModal()) {
		pDoc->m_sEventFileName = fileDlg.GetPathName();
		pDoc->LoadEventFile();
		// 打开文件成功，则切换到事件编辑模式
		m_bEditEventMode = TRUE;
		displaymap();
	}
	else {
		return;
	}
}

//██以下是各个按钮的Update函数█████████████████████████
void CMapeditView::OnUpdateEditEventMode(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bEditEventMode?1:0);
}
void CMapeditView::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	pCmdUI->SetCheck((pDoc->undo_st==FALSE)?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush1(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 1))?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush2(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 2))?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush3(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 3))?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush4(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 4))?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush5(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 5))?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush6(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 6))?1:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateBrush7(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(((cur_brush == 9))?10:0);
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdateCopy(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
void CMapeditView::OnUpdatePaste(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEditEventMode?FALSE:TRUE);
}
//██████████████████████████████████████

void CMapeditView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;
	
	CView::OnLButtonUp(nFlags, point);
}

void CMapeditView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	m_bLButtonDown = FALSE;
	m_bRButtonDown = FALSE;
	
	CView::OnRButtonUp(nFlags, point);
}

void CMapeditView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// 鼠标左键按住，拖动时为放置操作
	if (m_bLButtonDown)
	{
		SetOneCell (point.x, point.y, selected);
	}
	// 鼠标右键按住，拖动时为清除操作
	else if (m_bRButtonDown)
	{
		SetOneCell (point.x, point.y, 0);
	}
	
	CView::OnMouseMove(nFlags, point);
}

void CMapeditView::SetOneCell( int x, int y, int value ) 
{
	if ((x < 0) || (y < 0) || (x > EDIT_AREA_W) || (y > EDIT_AREA_H)) {
		return;
	}

	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int X = (center_x - 10 + x / 32);
	int Y = (center_y - 10 + y / 32);

	if ((X > MAP_GRID_W-1) || (X < 0) || (Y > MAP_GRID_H-1) || (Y < 0)) {
		return;
	}

	if (m_bEditEventMode) {
		if (pDoc->ReadEvt (X, Y) != value) {
			pDoc->WriteEvt (X, Y, value);
			displaymap();
		}
	}
	else {
		if (pDoc->ReadMap (X, Y) != value) {
			pDoc->WriteMap (X, Y, value);
			displaymap();
		}
	}
}

int CMapeditView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// 重要 ！！！下列代码禁用中文输入法
	// 如果不禁用输入法，快捷键ASDF默认输入中文
	DisableIME (this->GetSafeHwnd());

	CRect rc1(0, EDIT_AREA_H + 1, EDIT_AREA_W, EDIT_AREA_H + SCROLL_BAR_W);
	if (-1 == m_scoEditAreaH.Create(SBS_HORZ | WS_CHILD, rc1, this, IDR_EDIT_AREA_SCROLL_H))
		return -1;
	CRect rc2(EDIT_AREA_W + 1, 0, EDIT_AREA_W + SCROLL_BAR_W, EDIT_AREA_H);
	if (-1 == m_scoEditAreaV.Create(SBS_VERT | WS_CHILD, rc2, this, IDR_EDIT_AREA_SCROLL_V))
		return -1;
	CRect rc3(MAP_TILES_LEFT, MAP_TILES_TOP + MAP_TILES_H + 1,
		MAP_TILES_LEFT + MAP_TILES_W, MAP_TILES_TOP + MAP_TILES_H + SCROLL_BAR_W);
	if (-1 == m_scoMapTilesH.Create(SBS_HORZ | WS_CHILD, rc3, this, IDR_MAP_TILES_SCROLL_H))
		return -1;

	m_scoEditAreaH.ShowScrollBar(TRUE);
	m_scoEditAreaV.ShowScrollBar(TRUE);
	m_scoMapTilesH.ShowScrollBar(TRUE);

	m_scoEditAreaH.SetScrollRange(1, MAP_GRID_W);
	m_scoEditAreaV.SetScrollRange(1, MAP_GRID_H);
	m_scoMapTilesH.SetScrollRange(1, MAP_TILES_PAGE_COUNT);

	m_scoEditAreaH.SetScrollPos(center_x + 1);
	m_scoEditAreaV.SetScrollPos(center_y + 1);
	m_scoMapTilesH.SetScrollPos(tile_area);

	return 0;
}

void CMapeditView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (pScrollBar == &m_scoEditAreaV)
	{
		switch (nSBCode) {
		case SB_LINEUP:
			if (center_y > 0) {
				center_y --;
				nPos = center_y + 1;
			}
			else {
				return;
			}
			break;
		case SB_PAGEUP:
			if (center_y > 0) {
				center_y -= EDIT_PAGE_STEP;
				if (center_y < 0) center_y = 0;
				nPos = center_y + 1;
			}
			else {
				return;
			}
			break;
		case SB_LINEDOWN:
			if (center_y < MAP_GRID_H-1) {
				center_y ++;
				nPos = center_y + 1;
			}
			else {
				return;
			}
			break;
		case SB_PAGEDOWN:
			if (center_y < MAP_GRID_H-1) {
				center_y += EDIT_PAGE_STEP;
				if (center_y > MAP_GRID_H-1) {
					center_y = MAP_GRID_H-1;
				}
				nPos = center_y + 1;
			}
			else {
				return;
			}
			break;
		case SB_THUMBTRACK:
			if (nPos > 0 && nPos <= MAP_GRID_H) {
				center_y = nPos - 1;
			}
			break;
		}

		if (nPos > 0 && nPos <= MAP_GRID_H) {
			m_scoEditAreaV.SetScrollPos(nPos);
			displaymap();
		}
	}

	//CView::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CMapeditView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (pScrollBar == &m_scoEditAreaH)
	{
		switch (nSBCode) {
		case SB_LINELEFT:
			if (center_x > 0) {
				center_x --;
				nPos = center_x + 1;
			}
			else {
				return;
			}
			break;
		case SB_PAGELEFT:
			if (center_x > 0) {
				center_x -= EDIT_PAGE_STEP;
				if (center_x < 0) center_x = 0;
				nPos = center_x + 1;
			}
			else {
				return;
			}
			break;
		case SB_LINERIGHT:
			if (center_x < MAP_GRID_W-1) {
				center_x ++;
				nPos = center_x + 1;
			}
			else {
				return;
			}
			break;
		case SB_PAGERIGHT:
			if (center_x < MAP_GRID_W-1) {
				center_x += EDIT_PAGE_STEP;
				if (center_x > MAP_GRID_W-1) {
					center_x = MAP_GRID_W-1;
				}
				nPos = center_x + 1;
			}
			else {
				return;
			}
			break;
		case SB_THUMBTRACK:
			if (nPos > 0 && nPos <= MAP_GRID_W) {
				center_x = nPos - 1;
			}
			break;
		}

		if (nPos > 0 && nPos <= MAP_GRID_W) {
			m_scoEditAreaH.SetScrollPos(nPos);
			displaymap();
		}
	}

	if (pScrollBar == &m_scoMapTilesH)
	{
		switch (nSBCode) {
		case SB_LINELEFT:
		case SB_PAGELEFT:
			if (tile_area > 1) {
				nPos = tile_area - 1;
			}
			else {
				return;
			}
			break;
		case SB_LINERIGHT:
		case SB_PAGERIGHT:
			if (tile_area < MAP_TILES_PAGE_COUNT) {
				nPos = tile_area + 1;
			}
			else {
				return;
			}
			break;
		case SB_THUMBTRACK:
			break;
		}

		switch (nPos) {
		case 4:	selected = 31;	break;
		case 3:	selected = 21;	break;
		case 2:	selected = 11;	break;
		case 1:	selected = 1;	break;
		default:
			return;
		}
		tile_area = nPos;
		m_scoMapTilesH.SetScrollPos(nPos);
		displaymap();
	}
	//CView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CMapeditView::ExportPngFile(CString filename)
{
	CImage png;
	png.Create(TILE_W * MAP_GRID_W, TILE_H * MAP_GRID_H, 24);
	CDC dc;
	dc.Attach(png.GetDC());
		
	CMapeditDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	BeginWaitCursor();

	dc.StretchBlt(0, 0, TILE_W * MAP_GRID_W, TILE_H * MAP_GRID_H,
		&MiniDC, 0, 0, THUMBNAIL_W, THUMBNAIL_H, SRCCOPY);

	int a = 0;
	int b = 0;
	int i, m, n;
	for (m = 0; m < MAP_GRID_W; m++) {
		for (n = 0; n < MAP_GRID_H; n++) {
			i = pDoc->ReadMap(m, n);
			if (i > 0 && i <= MAP_TILES_NUM_MAX) {
				int sx = (((i-1) % MAP_TILES_LOGIC_W) * TILE_W);
				int sy = (((i-1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从0到800
				//pDC->StretchBlt(b,a,mini_tile_size,mini_tile_size,&TileDC,sx,sy,32,32,SRCCOPY);//绘制
				//StretchBlt函数虽然速度快，但是压缩效果太差，还不如截取部分进行拼图
				dc.BitBlt(b,a,TILE_W,TILE_H,&TileDC,sx,sy,SRCCOPY);//绘制
			}

			if (m_bEditEventMode) {
				//事件很小，看不清数字，统一绘制成黑色方块
				int e = pDoc->ReadEvt(m, n);
				if (e > 0 && e <= MAP_TILES_NUM_MAX) {
					int sx = (((e-1) % MAP_TILES_LOGIC_W) * TILE_W);
					int sy = (((e-1) / MAP_TILES_LOGIC_W) * TILE_H);//i的范围从0到800
					dc.BitBlt(b,a,TILE_W,TILE_H,&CodeDC,sx,sy,SRCCOPY);//绘制
				}
			}

			a += TILE_W;
		}
		a = 0;
		b += TILE_H;
	}

	png.Save(filename);
	dc.Detach();

	png.ReleaseDC();
	png.Destroy();
	EndWaitCursor();
}
//██程序结束位置███████████████████████████████
