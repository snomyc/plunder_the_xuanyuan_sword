// Npc.cpp: implementation of the CNpc class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Game.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNpc::CNpc( CGameMap* pGameMap, int nType, int x, int y )
	: CGameObject( pGameMap )
{
	m_pGameMap = pGameMap;
	m_nType = nType;
	m_x = x;
	m_y = y;

	// 考虑到派生类可能用到更大的类型
	if( nType > 10 )
		return;

	m_hDeadDC  = pGameMap->m_hNpcDeadDC;

	// 棘球特殊处理
	// 7：向上；8：向下；9：向左；10：向右；
	if( (m_nType > 6) && (m_nType < 11) )
	{
		m_w = 128;
		m_h = 128;

		switch( m_nType )
		{
		case 7:    m_wAct = NPC_ACT_GO_UP;      break;
		case 8:    m_wAct = NPC_ACT_GO_DOWN;    break;
		case 9:    m_wAct = NPC_ACT_GO_LEFT;    break;
		case 10:   m_wAct = NPC_ACT_GO_RIGHT;   break;
		}

		m_hImageDC = pGameMap->m_hNpcImageDC[ 6 ];

		// 设置动作序列
		m_psAction = NULL;
		m_nActCount = 0;
		m_nCurAct = 0;

		// 设置属性
		m_nLife = 1000;
		m_nLevel = 100;

		m_nStatus = 1;
		m_nGoDelay = 2;
		m_nHitHero = 0;

		return;
	}

	m_hImageDC = pGameMap->m_hNpcImageDC[ nType - 1 ];

	// 设置动作序列
	m_psAction = pGameMap->m_psNpcAction[ nType - 1 ];
	m_nActCount = strlen( m_psAction );
	m_nCurAct = 0;

	// 默认尺寸
	m_w = NPC_W;
	m_h = NPC_H;

	// 设置属性
	m_nLife = 1000;
	m_nLevel = nType * 5;

	m_wAct = NPC_ACT_GO_DOWN;
	m_nStatus = 1;
	m_nGoDelay = 8;
	m_nHitHero = 0;
}

CNpc::~CNpc()
{
}

// 绘制
void CNpc::Draw( HDC hDC )
{
	// 类型为0表示已经死亡
	if( m_nType == 0 )
		return;

	int nLeft = m_pGameMap->m_nLeft;
	int nTop  = m_pGameMap->m_nTop;
	int x  = m_x - nLeft;
	int y  = m_y - nTop;

	// 棘球单独绘制
	if( (m_nType > 6) && (m_nType < 11) )
	{
		TransBlt( hDC, x, y, m_w, m_h, m_hImageDC, 0, 0, COLORKEY );
		return;
	}

	// 绘制死亡动画
	if( m_wAct == NPC_ACT_DEAD )
	{
		TransBlt( hDC, x-(110-NPC_W)/2, y-(60-NPC_H)/2, 110, 60, m_hDeadDC, 0, 60*(m_nStatus-100), COLORKEY );
		m_nStatus ++;
		if( m_nStatus == 109 )
			m_nType = 0;
		return;
	}

	// 绘制活动的NPC
	if( !CanBeShown() )
		return;

	// 绘制攻击角色效果
	int hero_x, hero_y, hero_w, hero_h;
	m_pGameMap->m_pHero->GetHitTestRect( &hero_x, &hero_y, &hero_w, &hero_h );

	// 1>绘制到Hero的浅蓝色攻击线
	if( (m_wAct & NPC_ACT_FIRE) > 0 )
	{
		HANDLE hOldPen = SelectObject( hDC, CreatePen( PS_SOLID, 3, RGB(128,255,255)) );

		int nLeft = m_pGameMap->m_nLeft;
		int nTop  = m_pGameMap->m_nTop;
		int hero_center_x = hero_x - nLeft + hero_w / 2;
		int hero_center_y = hero_y - nTop  + hero_h / 2;

		int center_x = m_x - nLeft + m_w / 2;
		int center_y = m_y - nTop  + m_h / 2;

		::MoveToEx( hDC, hero_center_x, hero_center_y, NULL );
		::LineTo  ( hDC, center_x, center_y );

		DeleteObject( SelectObject( hDC, hOldPen ) );
	}

	// 2>在Hero位置绘制火花
	if( m_nHitHero > 0 )
	{
		m_nHitHero --;
		TransBlt( hDC, hero_x-nLeft, hero_y-nTop, NPC_W, NPC_H, m_hImageDC, m_nHitHero*32, 64, COLORKEY );
	}

	// 绘制NPC头上的血格
	BitBlt( hDC, x, y-6, m_w, 4, m_hImageDC, 0, 0, BLACKNESS );
	StretchBlt(hDC,x,y-6,m_nLife*m_w/1000,4,m_hImageDC,0,0,1,1,SRCCOPY);

	// 绘制NPC
	int nIndex = (m_nStatus % 20) - 1;
	if( (nIndex < 0) || (nIndex > 3) )
		return;

	int sx = npc_act_x[ nIndex ];
	int sy = npc_act_y[ nIndex ];

	if( m_nStatus > 20 )
		sy += 32;

	TransBlt( hDC, x, y, m_w, m_h, m_hImageDC, sx, sy, COLORKEY );
}

// 获得所在格子的坐标。每个精灵只能占一个格子的位置
// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
BOOL CNpc::GetGride( int* pX, int* pY )
{
	int iCenterX = m_x + NPC_CENTER_OFFSET_X;
	int iCenterY = m_y + NPC_CENTER_OFFSET_Y;

	if( (iCenterX < 0) || (iCenterX > TILE_W * GRIDE_W) )
		return FALSE;
	if( (iCenterY < 0) || (iCenterY > TILE_H * GRIDE_H) )
		return FALSE;

	*pX = iCenterX / TILE_W;
	*pY = iCenterY / TILE_H;
	return TRUE;
}

// 得到碰撞检测矩形
BOOL CNpc::GetHitTestRect( int* x, int* y, int* w, int* h )
{
	// 类型为0表示已经死亡
	if( (m_nType == 0) || (m_wAct == NPC_ACT_DEAD) )
		return FALSE;

	*x = m_x;
	*y = m_y;
	*w = m_w;
	*h = m_h;
	return TRUE;
}

// 判断是否与另外一个物体相交
BOOL CNpc::IsObjectCut( CGameObject* pObj )
{
	// 类型为0表示已经死亡
	if( (m_nType == 0) || (m_wAct == NPC_ACT_DEAD) )
		return FALSE;

	return CGameObject::IsObjectCut( pObj );
}

// 移动
void CNpc::Move( long lNow )
{
	// 类型为0表示已经死亡
	if( (m_nType == 0) || (m_wAct == NPC_ACT_DEAD) )
		return;

	// 棘球单独处理
	if( (m_nType > 6) && (m_nType < 11) )
	{
		// 棘球会不断的攻击角色
		// 判断是否与角色发生碰撞
		if( IsObjectCut( m_pGameMap->m_pHero ) )
			// 给角色以致命打击
			m_pGameMap->HitHero( 10000000 );

		// 1>行走动作的推进，动作未完成则拒绝接受新的动作
		if( (m_wAct & NPC_ACT_GO_COMPLETE) > 0 )
		{
			// 行走状态已经结束，设置下一个动作；
			switch( m_nType )
			{
			case 7:
				GoUp();
				break;
			case 8:
				GoDown();
				break;
			case 9:
				GoLeft();
				break;
			case 10:
				GoRight();
				break;
			}

			if( m_nStatus > 20 )
				m_nStatus -= 20;

			m_nGoDelay = 2;

			// 清除动作完成标志
			m_wAct &= ~NPC_ACT_GO_COMPLETE;

			// 移动到新位置
			MoveToNewPos( m_wAct & 0x07 );
		}

		// 2>推进动作
		if( (m_wAct & 0x07) > 0 )
		{
			if( m_nGoDelay > 0 )
			{
				m_nGoDelay --;

				if( m_nGoDelay == 1 )
					m_nStatus += 20;
				if( m_nGoDelay == 0 )
					m_wAct |= NPC_ACT_GO_COMPLETE;

				// 移动到新位置
				MoveToNewPos( m_wAct & 0x07 );
			}
		}

		return;
	}

	// 其他类型的处理
	// 移动活动的怪兽
	if( !CanBeShown() )
		return;

	// 将当前动作推进到下一个状态
	// 1>行走动作的推进，动作未完成则拒绝接受新的动作
	if( (m_wAct & NPC_ACT_GO_COMPLETE) > 0 )
	{
		// 行走状态已经结束。
		// 设置下一个动作；
		int nAct = GetNextAction();
		switch( nAct )
		{
		case '1':
			TurnLeft();
			break;
		case '2':
			TurnRight();
			break;
		case '3':
			GoForward();
			break;
		case '4':
			CloseUp();
			break;
		case '5':
			RunAway();
			break;
		case '6':
			Fire();
			break;
		default:
			CloseUp();
		}

		if( m_nStatus > 20 )
			m_nStatus -= 20;

		m_nGoDelay = 8;

		// 清除动作完成标志
		m_wAct &= ~NPC_ACT_GO_COMPLETE;

		// 移动到新位置
		MoveToNewPos( m_wAct & 0x07 );
	}

	// 2>推进动作
	if( (m_wAct & 0x07) > 0 )
	{
		if( m_nGoDelay > 0 )
		{
			m_nGoDelay --;

			if( m_nGoDelay == 4 )
				m_nStatus += 20;
			if( m_nGoDelay == 0 )
				m_wAct |= NPC_ACT_GO_COMPLETE;

			// 移动到新位置
			MoveToNewPos( m_wAct & 0x07 );
		}
	}

	// 3>推进进攻动作和受攻击动作
	if( (m_wAct & NPC_ACT_OTHER_COMPLETE) > 0 )
	{
		m_wAct &= ~NPC_ACT_FIRE;
		m_wAct &= ~NPC_ACT_BE_HIT;
		m_wAct &= ~NPC_ACT_OTHER_COMPLETE;
	}

	if( (m_wAct & NPC_ACT_FIRE) > 0 )
		m_wAct |= NPC_ACT_OTHER_COMPLETE;
	if( (m_wAct & NPC_ACT_BE_HIT) > 0 )
		m_wAct |= NPC_ACT_OTHER_COMPLETE;
}

// 移动到新位置
void CNpc::MoveToNewPos( WORD wAct )
{
	int nOld_x = m_x;
	int nOld_y = m_y;

	// (需要增加碰撞检测)
	int X; int Y;
	if( GetGride( &X, &Y ) )
	{
		switch( wAct )
		{
		case NPC_ACT_GO_UP:
			{
				if( Y > 0 )
					m_y -= NPC_STEP;
			}
			break;
		case NPC_ACT_GO_DOWN:
			{
				if( Y < GRIDE_H - 1 )
					m_y += NPC_STEP;
			}
			break;
		case NPC_ACT_GO_LEFT:
			{
				if( X > 0 )
					m_x -= NPC_STEP;
			}
			break;
		case NPC_ACT_GO_RIGHT:
			{
				if( X < GRIDE_W - 1 )
					m_x += NPC_STEP;
			}
			break;
		}
	}

	// 棘球处理比较特殊
	// 由于棘球尺寸大，占用16格，需要特殊检测
	if( (m_nType > 6) && (m_nType < 11) )
	{
		int i, X, Y, x, y;

		switch( m_nType )
		{
		case 7:    // 上行
			x = m_x + 1;
			y = m_y + 1;

			X = x / TILE_W;
			Y = y / TILE_H;

			for( i=0; i<4; i++ )
			{
				if( m_pGameMap->map[X+i][Y] > 200 )
				{
					m_x = nOld_x;
					m_y = nOld_y;
					return;
				}
			}

			break;
		case 8:    // 下行
			x = m_x + 1;
			y = m_y + 126;

			X = x / TILE_W;
			Y = y / TILE_H;

			for( i=0; i<4; i++ )
			{
				if( m_pGameMap->map[X+i][Y] > 200 )
				{
					m_x = nOld_x;
					m_y = nOld_y;
					return;
				}
			}

			break;
		case 9:    // 左行
			x = m_x + 1;
			y = m_y + 1;

			X = x / TILE_W;
			Y = y / TILE_H;

			for( i=0; i<4; i++ )
			{
				if( m_pGameMap->map[X][Y+i] > 200 )
				{
					m_x = nOld_x;
					m_y = nOld_y;
					return;
				}
			}

			break;
		case 10:   // 右行
			x = m_x + 126;
			y = m_y + 1;

			X = x / TILE_W;
			Y = y / TILE_H;

			for( i=0; i<4; i++ )
			{
				if( m_pGameMap->map[X][Y+i] > 200 )
				{
					m_x = nOld_x;
					m_y = nOld_y;
					return;
				}
			}

			break;
		}

		return;
	}

	// 判断前方是否可以行走。如果前方不能行走，则随机选择TurnLeft或者TurnRight。
	if( (m_pGameMap->HitWallTest(this)) ||
		((m_pGameMap->HitTest(this)) != NULL) )
	{
		m_x = nOld_x;
		m_y = nOld_y;

		if( rand() % 2 )
			TurnLeft();
		else
			TurnRight();

		return;
	}
}

// 得到下一个动作
int CNpc::GetNextAction()
{
	int nAct = m_psAction[m_nCurAct];

	m_nCurAct ++;
	if( m_nCurAct >= m_nActCount )
		m_nCurAct = 0;

	if( (nAct >= '1') && (nAct <= '6') )
		return nAct;

	return '0';
	// 返回一个不存在的动作，将使NPC回到一个默认动作
}

// 向左转
void CNpc::TurnLeft()
{
	WORD wAct = m_wAct & 0x07;
	switch( wAct )
	{
	case NPC_ACT_GO_UP:
		GoLeft();
		break;
	case NPC_ACT_GO_DOWN:
		GoRight();
		break;
	case NPC_ACT_GO_LEFT:
		GoDown();
		break;
	case NPC_ACT_GO_RIGHT:
		GoUp();
		break;
	}
}

// 向右转
void CNpc::TurnRight()
{
	WORD wAct = m_wAct & 0x07;
	switch( wAct )
	{
	case NPC_ACT_GO_UP:
		GoRight();
		break;
	case NPC_ACT_GO_DOWN:
		GoLeft();
		break;
	case NPC_ACT_GO_LEFT:
		GoUp();
		break;
	case NPC_ACT_GO_RIGHT:
		GoDown();
		break;
	}
}

// 向前走
void CNpc::GoForward()
{
	// 简单的清除“行走动作完成”标志
	m_wAct &= ~NPC_ACT_GO_COMPLETE;

/* 以下处理效果并不理想 */
/*
	// 判断是否处于角色的正前方。如果出于角色正前方，则躲避。
	// 1>构造主角正前方区域的碰撞检测矩形
	int area_x, area_y, area_w, area_h;
	if( !m_pGameMap->m_pHero->GetHitTestRect( &area_x, &area_y, &area_w, &area_h ) )
		return;

	DWORD hero_act = m_pGameMap->m_pHero->m_wAct;
	switch( hero_act )
	{
	case HERO_ACT_GO_UP:
		area_y -= GAME_AREA_H / 2;
		area_h += GAME_AREA_H / 2;
		break;
	case HERO_ACT_GO_DOWN:
		area_h += GAME_AREA_H / 2;
		break;
	case HERO_ACT_GO_LEFT:
		area_x -= GAME_AREA_W / 2;
		area_w += GAME_AREA_W / 2;
		break;
	case HERO_ACT_GO_RIGHT:
		area_w += GAME_AREA_W / 2;
		break;
	}

	// 2>如果处于角色的正前方，则随机选择TurnLeft或者TurnRight。
	if( IsAreaCut( m_x,m_y,m_w,m_h, area_x,area_y,area_w,area_h ) )
	{
		if( rand() % 2 )
			TurnLeft();
		else
			TurnRight();
	}
*/
}

// 靠近角色
void CNpc::CloseUp()
{
	int hero_x, hero_y, hero_w, hero_h;
	if( !m_pGameMap->m_pHero->GetHitTestRect( &hero_x, &hero_y, &hero_w, &hero_h ) )
		return;

	int hero_center_x = hero_x + hero_w / 2;
	int hero_center_y = hero_y + hero_h / 2;

	int center_x = m_x + m_w / 2;
	int center_y = m_y + m_h / 2;

	int tx = hero_center_x - center_x;
	int ty = hero_center_y - center_y;

	if( abs( tx ) > abs( ty ) )
	{
		// 横向移动
		if( tx > 0 )
			// 向右移动
			GoRight();
		else
			// 向左移动
			GoLeft();
	}
	else
	{
		// 纵向移动
		if( ty > 0 )
			// 向下移动
			GoDown();
		else
			// 向上移动
			GoUp();
	}
}

// 远离角色
void CNpc::RunAway()
{
	int hero_x, hero_y, hero_w, hero_h;
	if( !m_pGameMap->m_pHero->GetHitTestRect( &hero_x, &hero_y, &hero_w, &hero_h ) )
		return;

	int hero_center_x = hero_x + hero_w / 2;
	int hero_center_y = hero_y + hero_h / 2;

	int center_x = m_x + m_w / 2;
	int center_y = m_y + m_h / 2;

	int tx = hero_center_x - center_x;
	int ty = hero_center_y - center_y;

	if( abs( tx ) > abs( ty ) )
	{
		// 横向移动
		if( tx < 0 )
			// 向右移动
			GoRight();
		else
			// 向左移动
			GoLeft();
	}
	else
	{
		// 纵向移动
		if( ty < 0 )
			// 向下移动
			GoDown();
		else
			// 向上移动
			GoUp();
	}
}

// 攻击角色
void CNpc::Fire()
{
	// 判断角色是否在攻击范围内
	// 1>得到攻击距离
	double fire_distance = pdbFireDistance[ m_nType - 1 ];
	fire_distance *= fire_distance;

	int hero_x, hero_y, hero_w, hero_h;
	if( !m_pGameMap->m_pHero->GetHitTestRect( &hero_x, &hero_y, &hero_w, &hero_h ) )
		return;

	int hero_center_x = hero_x + hero_w / 2;
	int hero_center_y = hero_y + hero_h / 2;

	int center_x = m_x + m_w / 2;
	int center_y = m_y + m_h / 2;

	double tx = hero_center_x - center_x;
	double ty = hero_center_y - center_y;
	double real_distance = tx * tx + ty * ty;

	if( real_distance > fire_distance )
	{
		// 不在攻击范围之内
		GoForward();
		return;
	}

	// 2>设定动作为攻击，调用GameMap类的攻击角色函数
	m_wAct |= NPC_ACT_FIRE;
	m_nHitHero = 4;
	int nInjurePower = pnNpcInjure[ m_nType - 1 ];
	m_pGameMap->HitHero( nInjurePower );
}

// 遭受攻击
void CNpc::BeHit( int nPower )
{
	// 类型为0表示已经死亡
	if( m_nType == 0 )
		return;

	// 棘球不怕攻击
	if( (m_nType > 6) && (m_nType < 11) )
		return;

	if( m_nLife == 0 )
		return;

	int nInjure = nPower / m_nLevel;
	if( nInjure < 10 )
		nInjure = 10;

	m_nLife -= nInjure;

	m_wAct |= NPC_ACT_BE_HIT;

	// 死亡处理
	if( m_nLife <= 0 )
	{
		m_nLife = 0;
		m_wAct = NPC_ACT_DEAD;
		m_nStatus = 100;

		// 调用GameMap的函数给角色增加经验值
		int nExp = pnNpcExp[ m_nType - 1 ];
		m_pGameMap->AddExp( nExp );
	}
}

// 角色运动
void CNpc::GoUp()
{
	m_wAct &= 0xF8;
	m_wAct |= NPC_ACT_GO_UP;
	m_nStatus = 1;
}
void CNpc::GoDown()
{
	m_wAct &= 0xF8;
	m_wAct |= NPC_ACT_GO_DOWN;
	m_nStatus = 2;
}
void CNpc::GoLeft()
{
	m_wAct &= 0xF8;
	m_wAct |= NPC_ACT_GO_LEFT;
	m_nStatus = 3;
}
void CNpc::GoRight()
{
	m_wAct &= 0xF8;
	m_wAct |= NPC_ACT_GO_RIGHT;
	m_nStatus = 4;
}

//是不是棘球？
BOOL CNpc::IsSpikyBall()
{
	return ((m_nType > 6) && (m_nType < 11));
}

/* END */