//Scene.h			“场”的基类，用于初始化
//
#if !defined(__SCENE_H__)
#define __SCENE_H__

typedef struct
{
	int nActType;	//指令类型
	int nPara1;		//第一个参数
	int nPara2;		//第二个参数
	int nPara3;		//第三个参数
	int nPara4;		//第四个参数
	int nPara5;		//第五个参数
	int nPara6;		//第六个参数
}ACT;
//
typedef struct
{
	int nCluType;	//指令类型
	int nPara1;		//第一个参数
	int nPara2;		//第二个参数
	int nPara3;		//第三个参数
	int nPara4;		//第四个参数
}CLU;
//
class CScene
{
//
public:
	CScene();
	~CScene();
	BOOL  Open(char* FileName);
//
private:
	int   nID;		//游戏进程ID
	int   nType;	//场类型
	int   nActNum;	//过程阶段动作指令的数目
	int   nCurAct;	//0-based，当前动作指令
	ACT*  pAct;		//动作指令数组
	int   nCluNum;	//出口阶段计算指令的数目
	int   nCurClu;	//0-based，当前计算指令
	CLU*  pClu;		//计算指令数组
//
protected:
	BOOL  GetNextAct(ACT* pA);				//获得下一条动作指令。
	BOOL  CluAll(CGameData* pDB);			//执行出口过程的全部计算指令。
};

#endif
