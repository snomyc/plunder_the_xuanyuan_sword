//bmp.cpp

#include "stdafx.h"


//
CBmp::CBmp()
{
};
//
CBmp::~CBmp()
{
	DeleteDC( hMemDC );
	DeleteDC( hSMaskDC );
	DeleteDC( hDMaskDC );
	DeleteDC( hBufDC );
};
//
BOOL CBmp::InitBmp(HWND hwnd)
{
	x=0;y=0;cx=0;cy=0;
	nState = 0;
	bf.AlphaFormat = 0;
	bf.BlendFlags = 0;
	bf.BlendOp = AC_SRC_OVER;
	bf.SourceConstantAlpha = 0;
	HDC hdc = ::GetDC( hwnd );
	hMemDC = ::CreateCompatibleDC( hdc );
	hSMaskDC = ::CreateCompatibleDC( hdc );
	hDMaskDC = ::CreateCompatibleDC( hdc );
	hBufDC = ::CreateCompatibleDC( hdc );
	::ReleaseDC( hwnd, hdc);
	return TRUE;
};
//
BOOL CBmp::Open(char *FileName,int sx,int sy,int scx,int scy)
{
	HBITMAP hBmp1 = (HBITMAP)LoadImage( NULL, FileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
	DeleteObject( ::SelectObject(hMemDC,hBmp1) );
	HBITMAP hBmp2 = ::CreateCompatibleBitmap(hMemDC,scx,scy);
	DeleteObject( ::SelectObject(hBufDC,hBmp2) );
	//
	x = sx;
	y = sy;
	cx = scx;
	cy = scy;
//	nState = 1;
	nState = 6;
	return TRUE;
};
//
void CBmp::Paint( HWND hwnd, HDC hdc )
{
	if( nState == 6 )
	{	//正常显示状态
		TransBlt( hdc,x,y,cx,cy,hMemDC,0,0,RGB(255,0,255) );
	}
};//Paint()函数结束
//
BOOL CBmp::IsBusy()
{
	if( nState==0 || nState==6 )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
};
//
BOOL CBmp::Close()
{
//	nState = 7;
	nState = 0;
	return TRUE;
};
//the end.